# cmake-generator
This command line application came to fruition out of the desperate hopes and dreams of a few people looking to softly lighten their suffering while using c++. 

# STAR THE REPO

![](./source/images/average_cpp_learner.png)


## Built with
tears\
sadness\
cmake > 3.2 \
g++ > 13 \
make \
vscode

## What it do!
todo!


## Roadmap
- [ ] more scuff
- [ ] package manager
- [ ] tests
- [ ] 

### Your friend in the world of C/C++
cmake-generator is a command line utility used to expedite building modern c/c++ applications.

- [x] Simple project initialization
- [ ] Simple dependency management
- [ ] Existing cmake project migration
