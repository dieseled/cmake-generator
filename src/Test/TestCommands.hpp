
#ifdef TEST
#pragma once
#include "../Command/Command.hpp"
#include <catch2/catch_test_macros.hpp>
#include <cxxopts.hpp>
#include <memory>


namespace TestCommands {

  bool testCommandInit();
}
#endif

